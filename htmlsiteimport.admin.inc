<?php

/**
 * @file
 *
 */

// DEFINITIONS FORMS

function htmlsiteimport_definitions() {
  $form = array();
  
  $form['baseurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Base url'),
    '#size' => 50,
    '#default_value' => variable_get('htmlsiteimport_baseurl', ''),
  );
  $form['depth'] = array(
    '#type' => 'textfield',
    '#title' => t('Depth'),
    '#size' => 4,
    '#default_value' => variable_get('htmlsiteimport_depth', 9)
  );
  
  // Existing definitions
  $definitions = db_query('SELECT * FROM {htmlsiteimport} ORDER BY weight ASC, id ASC')->fetchAllAssoc('id');
  $definitions['new'] = (object) array(
    'entity_type' => 'node',
    'bundle' => '',
    'weight' => 0,
    'urlmask' => '',
    'qpmask' => '',
  );
  $maxweight = 0;
  
  if (!empty($definitions)) {
    $form['definitions'] = array(
      '#type' => 'container',
      '#theme' => 'htmlsiteimport_table',
      '#tree' => TRUE,
      '#header' => array(
        'entity_type' => t('Entity type'),
        'bundle' => t('Bundle'),
        'weight' => t('Weight'),
        'urlmask' => t('URL Mask'),
        'qpmask' => t('QueryPath'),
        'edit' => t('Fields'),
        'delete' => t('Delete'),
      ),
    );

    foreach ($definitions as $id => $definition) {
      if (!empty($definition->fields)) {
        $definition->fields = unserialize($definition->fields);
      }
      $maxweight = max($maxweight, $definition->weight);
      $form['definitions'][$id] = array(
        '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
        'entity_type' => array(
          '#type' => 'textfield',
          '#title' => t('Entity type'),
          '#title_display' => 'invisible',
          '#default_value' => $definition->entity_type,
          '#size' => 6,
          '#disabled' => !empty($definition->fields),
        ),
        'bundle' => array(
          '#type' => 'textfield',
          '#title' => t('Bundle'),
          '#title_display' => 'invisible',
          '#default_value' => $definition->bundle,
          '#size' => 9,
          '#disabled' => !empty($definition->fields),
        ),
        'weight' => array(
          '#type' => 'textfield',
          '#title' => t('Weight for @id', array('@id' => $id)),
          '#title_display' => 'invisible',
          '#default_value' => $definition->weight,
          '#size' => 3,
          '#attributes' => array('class' => array('field-weight')),
        ),
        'urlmask' => array(
          '#type' => 'textfield',
          '#title' => t('URL Mask'),
          '#title_display' => 'invisible',
          '#default_value' => $definition->urlmask,
          '#size' => 20,
        ),
        'qpmask' => array(
          '#type' => 'textfield',
          '#title' => t('QueryPath Mask'),
          '#title_display' => 'invisible',
          '#default_value' => $definition->qpmask,
          '#size' => 20,
        ),
      );
      if ($id != 'new') {
        $form['definitions'][$id]['id'] = array(
          '#type' => 'value',
          '#value' => $definition->id,
          '#table_hidden' => TRUE,
        );
        $form['definitions'][$id]['existing'] = array(
          '#type' => 'value',
          '#value' => (array) $definition,
          '#table_hidden' => TRUE,
        );
        $fields_text = array();
        if (!empty($definition->fields) && is_array($definition->fields)) {
          foreach ($definition->fields as $field_def) {
            $fields_text[] = preg_replace('/^[^:]+:*/', '', $field_def['field']);
          }
        }
        $fields_text = implode(', ', $fields_text);
        $form['definitions'][$id]['fields'] = array(
          '#markup' => $fields_text . ' ' . l(t('edit'), 'admin/content/htmlsiteimport/definitions/' . $id),
        );
        $form['definitions'][$id]['delete'] = array(
          '#type' => 'checkbox',
          '#title' => t('Delete'),
          '#title_display' => 'invisible',
          '#default_value' => FALSE,
        );
        
      }
    }
  }
  $form['definitions']['new']['weight']['#default_value'] = $maxweight + 1;
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function htmlsiteimport_definitions_submit(&$form, &$form_state) {
  variable_set('htmlsiteimport_baseurl', $form_state['values']['baseurl']);
  variable_set('htmlsiteimport_depth', $form_state['values']['depth']);
  if (!db_query('SELECT id FROM {htmlsiteimport_items} WHERE url = ?', array($form_state['values']['baseurl']))->fetchField()) {
    $item = array(
      'url' => $form_state['values']['baseurl'],
      'depth' => 0,
    );
    htmlsiteimport_item_save($item);
  }
  if ($form_state['values']['definitions']['new']['entity_type'] && $form_state['values']['definitions']['new']['bundle']) {
    drupal_write_record('htmlsiteimport', $form_state['values']['definitions']['new']);
  }
  unset ($form_state['values']['definitions']['new']);
  if (!empty($form_state['values']['definitions'])) {
    foreach($form_state['values']['definitions'] as $id => $definition) {
      if ($definition['delete'] == TRUE) {
        db_delete('htmlsiteimport')
          ->condition('id', $id)
          ->execute();
      }
      elseif ($existing = $definition['existing']) {
        unset($definition['existing']);
        if ($changes = array_diff($definition, $existing)) {
          $changes['id'] = $id;
          drupal_write_record('htmlsiteimport', $changes, array('id'));
        }
      }
    }
  }
}

function htmlsiteimport_definition_detail($form, $form_state, $definition) {
  $form = array(
    'id' => array(
      '#type' => 'value',
      '#value' => $definition->id,
    ),
    'entity_type' => array(
      '#type' => 'textfield',
      '#title' => t('Entity type'),
      '#default_value' => $definition->entity_type,
      '#size' => 6,
      '#disabled' => !empty($definition->fields),
    ),
    'bundle' => array(
      '#type' => 'textfield',
      '#title' => t('Bundle'),
      '#default_value' => $definition->bundle,
      '#size' => 9,
      '#disabled' => !empty($definition->fields),
    ),
    'weight' => array(
      '#type' => 'value',
      '#value' => $definition->weight,
    ),
    'urlmask' => array(
      '#type' => 'textfield',
      '#title' => t('URL Mask'),
      '#default_value' => $definition->urlmask,
      '#size' => 60,
    ),
    'qpmask' => array(
      '#type' => 'textfield',
      '#title' => t('QueryPath Mask'),
      '#default_value' => $definition->qpmask,
      '#size' => 60,
    ),
    'fields' => array(
      '#tree' => TRUE,
      '#type' => 'container',
      '#theme' => 'htmlsiteimport_table',
      '#header' => array(
        'field' => t('Field'),
        'weight' => t('Weight'),
        'qp' => t('QueryPath'),
        'selector' => t('QP Selector'),
        'multiple' => t('Multiple'),
        'process' => t('Process'),
        'default' => t('Default'),
      ),
    ),
  );
  
  // Setup new field definition
  $definition->fields[] = array(
    'field' => 0,
    'weight' => 0,
    'qp' => '',
    'multiple' => 0,
    'process' => array(),
    'default' => '',
  );
  
  $field_options[0] = t('none / delete');
  $field_options['none'] = t('none : process then discard');
  $instances = field_info_instances($definition->entity_type, $definition->bundle);
  foreach ($instances as $field_name => $instance) {
    $fields[$field_name] = field_info_field($field_name);
    foreach ($fields[$field_name]['columns'] as $column => $info) {
      $field_options["field:$field_name:$column"] = t("!field : !column (!type field)", array(
        '!field' => $field_name,
        '!column' => $column,
        '!type' => $fields[$field_name]['type'],
      ));
    }
  }
  $form['#field_options'] = $field_options;
  $entity_info = entity_get_info($definition->entity_type);
  foreach ($entity_info['schema_fields_sql']['base table'] as $property) {
    $field_options["property:$property"] = t('!property (property)', array('!property' => $property));
  }
  
  $maxweight = 0;
  foreach ($definition->fields as $settings) {
    $maxweight = max(array($maxweight, $settings['weight']));
    $form['fields'][] = array(
      '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
      'field' => array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['field'],
        '#options' => $field_options,
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['field'] == 0 ? $maxweight + 1 : $settings['weight'],
        '#size' => 3,
        '#attributes' => array('class' => array('field-weight')),
      ),
      'qp' => array(
        '#type' => 'textfield',
        '#title' => t('QueryPath'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['qp'],
        '#size' => 20
      ),
      'selector' => array(
        '#type' => 'select',
        '#options' => array(
          'text' => 'text()',
          'html' => 'html()',
          'innerHTML' => 'innerHTML()',
          'href' => 'attr("href")',
          'value' => 'attr("value")',
          'alt' => 'attr("alt")',
          'title' => 'attr("title")',
          'src' => 'attr("src")',
          'id' => 'attr("id")',
          'class' => 'attr("class")',
          'content' => 'attr("content")',
        ),
        '#title' => t('Selector'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($settings['selector']) ? $settings['selector'] : 'text',
      ),
      'multiple' => array(
        '#type' => 'select',
        '#title' => t('Multiple'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['multiple'],
        '#options' => array(
          'first' => t('First value'),
          'split' => t('Split'),
          'concatenate' => t('Concatenate'),
        ),
      ),
      'process' => array(
        '#type' => 'checkboxes',
        '#title' => t('Process'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['process'],
        '#options' => array(
          'images' => t('Import images'),
          'links' => t('Import linked files'),
          'media' => t('Import external media'),
          'convert_timestamp' => t('Convert to timestamp'),
          'utf8' => t('Convert to UTF-8'),
          'convert_username' => t('Convert name to uid'),
          'regex_mask' => t('Regex mask (advanced!)'),
        ),
      ),
      'default' => array(
        '#type' => 'textfield',
        '#title' => t('Default'),
        '#title_display' => 'invisible',
        '#default_value' => $settings['default'] ?: '',
        '#size' => 10
      ),
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['addnodefields'] = array(
    '#type' => 'submit',
    '#value' => t('Add node fields'),
  );
  
  $form['fields_export'] = array(
    '#markup' => '<textarea readonly title="Export" rows=3 style="border:1px solid lightgray; width:100%; background:#eee;">' . serialize($definition->fields) . '</textarea>'
  );
  $form['fields_import'] = array(
    '#type' => 'textarea',
    '#title' => t('Import'),
    '#default_value' => '',
    '#rows' => 3,
  );
  
  return $form;
}

function htmlsiteimport_definition_detail_submit(&$form, &$form_state) {
  $weights = $fieldnames = array();
  $maxweight = 0;
  unset($form_state['values']['fields_export']);
  foreach ($form_state['values']['fields'] as $index => $field_def) {
    if (!$field_def['field']) {
      unset($form_state['values']['fields'][$index]);
    }
    else {
      $maxweight = max($maxweight, $field_def['weight']);
      $weights[$index] = $field_def['weight'];
      $fieldnames[$index] = $field_def['field'];
    }
  }
  if (!empty($form_state['values']['fields_import']) && $more_fields = @unserialize($form_state['values']['fields_import'])) {
    foreach ($more_fields as $index => $field_def) {
      if (in_array($field_def['field'], array_keys($form['#field_options'])) && 
        !in_array($field_def['field'], $fieldnames)) {
        $maxweight = max($maxweight, $field_def['weight']);
        $weights[] = $field_def['weight'];
        $fieldnames[] = $field_def['field'];
        $form_state['values']['fields'][] = $field_def;
      }
    }
  }
  unset($form_state['values']['fields_import']);
  array_multisort($weights, SORT_ASC, $fieldnames, SORT_DESC, $form_state['values']['fields']);
  if ($form_state['clicked_button']['#value'] == t('Add node fields')) {
    foreach (htmlsiteimport_default_node_fields() as $field_def) {
      if (in_array($field_def['field'], array_keys($form['#field_options'])) && 
          !in_array($field_def['field'], $fieldnames)) {
        $maxweight++;
        $field_def['weight'] = $maxweight;
        $form_state['values']['fields'][] = $field_def;
      }
    }
  }
  drupal_write_record('htmlsiteimport', $form_state['values'], array('id'));
}

// ITEMS FORMS

function htmlsiteimport_items() {
  $form = array();
  
  $header = array(
    'url' => array('data' => t('Original URL'), 'field' => 'i.url'),
    'depth' => array('data' => t('Depth'), 'field' => 'i.depth'),
    'definition' => array('data' => t('Import definition'), 'field' => 'i.definition'),
    'crawled' => array('data' => t('Crawled'), 'field' => 'i.crawled'),
    'imported' => array('data' => t('Imported'), 'field' => 'i.imported'),
  );
  
  $filter_results = str_replace('*', '%', variable_get('htmlsiteimport_filter', '')) ?: '%';
  $show_ignored = variable_get('htmlsiteimport_show_ignored', FALSE) ? 1 : 0;
  
  $items = db_select('htmlsiteimport_items', 'i')->extend('PagerDefault')->extend('TableSort')
    ->fields('i')
    ->limit(50)
    ->condition('i.url', $filter_results, 'LIKE')
    ->condition('i.ignored', $show_ignored)
    ->orderByHeader($header)
    ->execute()
    ->fetchAllAssoc('id');
  
  if (!empty($items)) {
    $form['items'] = array(
      '#tree' => TRUE,
      '#type' => 'tableselect',
      '#header' => $header,
    );
    foreach ($items as $id => $item) {
      $form['items']['#options'][$id] = array(
        '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
        'url' => l(check_plain($item->url), check_url($item->url)),
        'depth' => check_plain($item->depth),
        'definition' => !empty($item->entity_type) ? format_string('@type:@bundle @id', array(
          '@type' => $item->entity_type,
          '@bundle' => $item->bundle,
          '@id' => $item->entity_id
        )) : '',
        'crawled' => $item->crawled ? format_date($item->crawled, 'short') : '',
        'imported' => $item->imported ? format_date($item->imported, 'short') : '',
      );
      if ($item->link) {
        $form['items']['#options'][$id]['definition'] = l($form['items']['#options'][$id]['definition'], $item->link);
      }
    }
  }
  $form['pager'] = array(
    '#markup' => theme('pager'),
  );
  
  
  $form['action'] = array(
    '#type' => 'select',
    '#title' => t('With selected'),
    '#options' => array(
      'crawl' => t('Crawl'),
      'parse' => t('Parse (reset definition, then import)'),
      'import' => t('Import'),
      'ignored' => t('Ignore'),
      'delete' => t('Delete'),
    ),
  );
  
  $form['filter_results'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter results by url'),
    '#default_value' => variable_get('htmlsiteimport_filter', ''),
  );
  $form['show_ignored'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show previously ignored entries'),
    '#default_value' => variable_get('htmlsiteimport_show_ignored', FALSE),
  );
    
//  $form['crawlall'] = array(
//    '#type' => 'submit',
//    '#value' => t('Crawl!!!'),
//  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

function htmlsiteimport_items_submit(&$form, &$form_state) {
  $action = $form_state['values']['action'];
  variable_set('htmlsiteimport_filter', $form_state['values']['filter_results']);
  variable_set('htmlsiteimport_show_ignored', $form_state['values']['show_ignored']);
  foreach (array_filter($form_state['values']['items']) as $id) {
    $item = htmlsiteimport_item_load($id);
    if ($action == 'crawl') {
      htmlsiteimport_crawl($item);
      htmlsiteimport_item_save($item);
    }
    elseif ($action == 'parse' || $action == 'import' && empty($item->definition)) {
      htmlsiteimport_parse($item);
      htmlsiteimport_item_save($item);
    }
    elseif ($action == 'import') {
      htmlsiteimport_import($item);
      htmlsiteimport_item_save($item);
    }
    elseif ($action == 'delete') {
      db_delete('htmlsiteimport_items')->condition('id', $id)->execute();
    }
    elseif ($action == 'ignored') {
      $item->ignored = TRUE;
      htmlsiteimport_item_save($item);
    }
  }
}

function htmlsiteimport_item_detail($form, $form_state, $item) {
  $form = array();
  
  return $form;
}

// THEME FUNCTION FOR FORM TABLE

function theme_htmlsiteimport_table(&$vars) {
  $element = $vars['form'];
  $table = array();
  
  $table['header'] = $element['#header'];
  $table['attributes'] = array();
  if (!empty($element['#attributes'])) {
    $table['attributes'] = $element['#attributes'];
  }
  $table['attributes']['id'] = 'htmlsiteimport-table';
  
  foreach (element_children($element) as $row_key) {
    $rows[$row_key] = array();
    $hidden = '';
    if (!empty($element[$row_key]['#attributes'])) {
      $rows[$row_key] += $element[$row_key]['#attributes'];
    }
    foreach (element_children($element[$row_key]) as $cell_key) {
      if (empty($vars['form'][$row_key][$cell_key]['#table_hidden'])) {
        $rows[$row_key]['data'][$cell_key]['data'] = drupal_render($vars['form'][$row_key][$cell_key]);
      }
      else {
        $hidden .= drupal_render($vars['form'][$row_key][$cell_key]);
      }
    }
    if (!empty($hidden)) {
      $rows[$row_key]['data']['hidden'] = array(
        'class' => 'element-invisible',
        'data' => $hidden,
      );
    }
  }
  $table['rows'] = $rows;
  $table['sticky'] = TRUE;
  $table['empty'] = t('None found.');
  $table['caption'] = !empty($element['#title']) ? $element['#title'] : '';
  $table['colgroups'] = array();
  drupal_add_tabledrag('htmlsiteimport-table', 'order', 'sibling', 'field-weight');
  
  return theme_table($table);
}

function htmlsiteimport_batch_crawl(&$context) {
  if (empty($context['sandbox']['context'])) {
    $context['sandbox']['progress'] = 0;
  }
  
}

// CRAWLING FUNCTIONS

function htmlsiteimport_crawl(&$item) {
  $baseurl = variable_get('htmlsiteimport_baseurl', '');
  $baseparts = parse_url($baseurl);
  $basehost = $baseparts['host'];

  $dom = new DOMDocument('1.0');
  @$dom->loadHTMLFile($item->url);
  
  $item->html = $dom->saveHTML();
  $item->crawled = time();
  
  htmlsiteimport_parse($item);

  if ($item->depth < variable_get('htmlsiteimport_depth', 9)) {
    $anchors = $dom->getElementsByTagName('a');
    foreach ($anchors as $element) {
      $href = $element->getAttribute('href');
      $href = preg_replace('|#[^/]*$|', '', rtrim($href, '/'));
      $href = htmlsiteimport_get_absolute_url($href, $item->url);
      $parts = parse_url($href);
      if (empty($parts['host']) || $parts['host'] != $basehost) {
        continue;
      }
      if (!db_query('SELECT url FROM htmlsiteimport_items WHERE url = :url', array('url' => $href))->fetchField()) {
        db_insert('htmlsiteimport_items')
          ->fields(array(
            'url' => $href,
            'depth' => $item->depth + 1,
          ))
          ->execute();
      }
    }
  }
}

function htmlsiteimport_parse(&$item) {
  $imported = FALSE;
  $definitions = db_query('SELECT * FROM {htmlsiteimport} ORDER BY weight ASC, id ASC')->fetchAllAssoc('id');
  if (!empty($definitions) && is_array($definitions)) {
    if (!$qp = _htmlsiteimport_qp($item)) {
      return FALSE;
    }
    $baseurl = variable_get('htmlsiteimport_baseurl', '');
    foreach ($definitions as $id => $definition) {
      if (drupal_match_path($item->url, $baseurl . '/' . ltrim($definition->urlmask, '/')) || // Test the path matching
          (!empty($definition->qpmask) && $qp->find($definition->qpmask))) {      // Test the Querypath matching
        $item->definition = $definition->id;
        $item->entity_type = $definition->entity_type;
        $item->bundle = $definition->bundle;

        $imported = htmlsiteimport_import($item, $qp);
        break;

      }
    }
  }
  
  return !empty($imported);
  
}

function htmlsiteimport_import(&$item, QueryPath $qp = NULL) {
  
  // Get the applicable definition
  if (empty($item->definition)) {
    return FALSE;
  }
  $definition = htmlsiteimport_definition_load($item->definition);
  
  // Get the Querypath object if it was not provided
  if (is_null($qp)) {
    if (!$qp = _htmlsiteimport_qp($item)) {
      return FALSE;
    }
  }
  
  $entity_info = entity_get_info($item->entity_type);
  
  if (!empty($item->entity_id)) {
    $entity = entity_load($item->entity_type, array($item->entity_id));
    if (!empty($entity[$item->entity_id]) && is_object($entity = $entity[$item->entity_id])) {
      if ($entity->{$entity_info['entity keys']['bundle']} != $item->bundle) {
        entity_delete($item->entity_type, $item->entity_id);
        unset($entity);
      }
    }
    else {
      $item->entity_id = NULL;
      $item->link = NULL;
    }
  }
  if (empty($entity) || !is_object($entity)) {
    $entity = new stdClass;
    $entity->{$entity_info['entity keys']['bundle']} = $item->bundle ?: $item->entity_type;
  }
  
  // Parse through fields
  foreach ($definition->fields as $def) {
    $field = explode(':', $def['field']);
    
    $value = $def['multiple'] == 'split' ? array() : '';
    
    if ($def['multiple'] == 'first') {
      $value = htmlsiteimport_get_value($qp->branch($def['qp'] . ':first'), $def, $item->url);
    }
    else {
      foreach($qp->branch($def['qp']) as $element) {
        $temp = htmlsiteimport_get_value($element, $def, $item->url);
        if ($def['multiple'] == 'split') {
          if (!is_array($temp)) {
            $value[] = $temp;
          }
          else {
            $value = array_merge($value, $temp);
          }
        }
        elseif ($def['multiple'] == 'concatenate') {
          if (is_array($temp)) {
            $temp = implode('', $temp);
          }
          $value .= $temp;
        }
      }
    }
    if (is_array($value)) {
      $value = array_filter($value);
    }
    
    if ($field[0] == 'none' || empty($value) || is_null($value)) {
      // Discard results in this case
    }
    elseif ($field[0] == 'property') {
      $entity->{$field[1]} = $value;
    }
    elseif ($field[0] == 'field' && !is_array($value)) {
      $entity->{$field[1]}['und'][0][$field[2]] = $value;
    }
    elseif ($field[0] == 'field' && is_array($value)) {
      foreach($value as $k => $v) {
        $entity->{$field[1]}['und'][$k][$field[2]] = $value[$k];
      }
    }
  }
  
  if (!empty($entity_info['entity keys']['language'])) {
    $entity->{$entity_info['entity keys']['language']} = language_default('language');
  }
  if ($success = _htmlsiteimport_entity_save($definition->entity_type, $entity)) {
    $item->imported = time();
    $item->entity_id = $entity->{$entity_info['entity keys']['id']};
    if (function_exists($entity_info['uri callback'])) {
      $entity_path = $entity_info['uri callback']($entity);
    }
    $item->link = $entity_path['path'];
    htmlsiteimport_item_save($item);
    if (!empty($item->link) && module_exists('redirect')) {
      _htmlsiteimport_redirect($item->url, $item->link);
    }
    return $success;
  }  
}

// VALUE FUNCTIONS

function htmlsiteimport_get_value(QueryPath $element, $field_def, $url) {
  $field = explode(':', $field_def['field']);
  $field_info = array();
  if ($field[0] == 'field') {
    $field_info = field_info_field($field[1]);
  }
  
  if (!isset($field_def['default'])) {
    $field_def['default'] = '';
  }
  
  if (empty($field_def['qp'])) {
    $value = $field_def['default'];
  }
  else {
    if (in_array($field_def['selector'], array('text', 'html', 'innerHTML'))) {
      $value = $element->$field_def['selector']() ?: $field_def['default'];
    }
    else {
      $value = $element->attr($field_def['selector']) ?: $field_def['default'];
    }
  }
  
  $value = trim($value);
  if ($field_def['process']['regex_mask'] && !empty($field_def['default'])) {
    $value = preg_replace('~.*' . $field_def['default'] . '.*~', '$1', $value);
  }
  
  if ($field_def['process']['utf8']) {
    $value = mb_convert_encoding($value, 'UTF-8');
  }
  
  if ($field_def['process']['convert_timestamp']) {
    if ($time = strtotime($value)) {
      $value = $time;
    }
  }
  
  if ($field_def['process']['convert_username']) {
    $value = db_query('select uid from {users} where name = ?', array($value))->fetchField() ?: 0;
  }
  
  if (!empty($field_info['type']) && $field_info['type'] == 'taxonomy_term_reference') {
    return htmlsiteimport_create_taxonomy_terms($value, $field_info);
  }
  
  if ($field_def['process']['media'] && module_exists('media_internet')) {
    if (in_array($field_def['selector'], array('href', 'src'))) {
      if ($file = htmlsiteimport_extract_media($value, $url)) {
        return $file->fid;
      }
    }
    else {
      foreach ($element->branch('iframe') as $part) {
        if ($file = htmlsiteimport_extract_media($part->attr('src'), $url)) {
          $tag = htmlsiteimport_get_media_tag($file);
          $value = str_replace($part->html(), $tag, $value);
        }
      }
    }
  }
  if ($field_def['process']['links'] || $field_def['process']['images']) {
    $baseurl = variable_get('htmlsiteimport_baseurl', '');
    if (in_array($field_def['selector'], array('href', 'src'))) {
      if (htmlsiteimport_check_file_href($value)) {
        if ($file = htmlsiteimport_extract_file($value, $url)) {
          return $file->fid;
        }
        else {
          return NULL;
        }
      }
    }
    else {
      if ($field_def['process']['images']) {
        foreach ($element->branch('img') as $part) {
          $src = $part->attr('src');
          $abs_src = htmlsiteimport_get_absolute_url($src, $url);
          if (drupal_match_path($abs_src, $baseurl . '*')) {
            if ($file = htmlsiteimport_extract_file($abs_src, $url)) {
              $tag = htmlsiteimport_get_media_tag($file);
              $value = str_replace($part->html(), $tag, $value);
            }
          }
        }
      }
      if ($field_def['process']['links']) {
        foreach ($element->branch('a')->get(NULL, TRUE) as $part) {
          $href = $part->getAttribute('href');
          $abs_href = htmlsiteimport_get_absolute_url($href, $url);
          if (drupal_match_path($abs_href, $baseurl . '*') && 
            htmlsiteimport_check_file_href($abs_href)) {
            if ($file = htmlsiteimport_extract_file($abs_href)) {
              $value = str_replace($href, file_create_url($file->uri), $value);
            }
          }
        }
      }
    }
  }
  
  return $value;
}

function htmlsiteimport_check_file_href($href) {
  return preg_match('#[^/]/[^/].+\..+$(?<!tml|htm|xml|php)#', $href);
}

function htmlsiteimport_extract_file($href, $url) {
  $href = htmlsiteimport_get_absolute_url($href, $url);
  if ($file = _htmlsiteimport_file_exists($href)) {
    return $file;
  }
  if (module_exists('media')) {
    try {
      // Save the remote file
      $provider = media_internet_get_provider($href);
      // Providers decide if they need to save locally or somewhere else.
      // This method returns a file object
      $file = $provider->save();
    }
    catch (MediaInternetNoHandlerException $e) {
      // then try the default file handling
    }
    catch (Exception $e) {
      return FALSE;
    }
  }
  
  if (module_exists('file')) {
    // TODO
  }
  
  if (!is_object($file) || !$file->fid) {
    return FALSE;
  }
  if ($file->filename != rawurldecode($file->filename)) {
    $file = file_move($file, 'public://' . rawurldecode($file->filename));
  }
  return $file;
}

function htmlsiteimport_extract_media($href, $url, $tag = FALSE) {
  if ($file = htmlsiteimport_extract_file($href)) {
    return $file;
  }
}

function htmlsiteimport_get_media_tag($file) {
  return '[[{"type":"media","view_mode":"media_original","fid":"' . $file->fid . '"}]]';
}

function htmlsiteimport_create_taxonomy_terms($value, $field_info) {
  foreach ($field_info['settings']['allowed_values'] as $tree) {
    if ($vocabulary = taxonomy_vocabulary_machine_name_load($tree['vocabulary'])) {
      $vocabularies[$vocabulary->vid] = $vocabulary;
    }
  }
  $typed_terms = explode(',', $value);
  $value = array();
  foreach ($typed_terms as $typed_term) {
    // See if the term exists in the chosen vocabulary and return the tid;
    // otherwise, create a new 'autocreate' term for insert/update.
    if ($possibilities = taxonomy_term_load_multiple(array(), array('name' => trim($typed_term), 'vid' => array_keys($vocabularies)))) {
      $term = array_pop($possibilities);
      $value[] = $term->tid;
    }
    else {
      $vocabulary = reset($vocabularies);
      $term = (object) array(
        'vid' => $vocabulary->vid,
        'name' => $typed_term,
        'vocabulary_machine_name' => $vocabulary->machine_name,
      );
      taxonomy_term_save($term);
      $value[] = $term->tid;
    }
  }
  return $value;
}

// HELPER FUNCTIONS
function htmlsiteimport_get_absolute_url($href, $baseurl = NULL) {
  if (0 !== strpos($href, 'http') && 0!== strpos($href, 'mailto:')) {
    $baseurl = $baseurl ?: variable_get('htmlsiteimport_baseurl', '');
    $path = '/' . ltrim($href, '/');
    if (extension_loaded('http')) {
      $href = http_build_url($baseurl, array('path' => $path));
    } else {
      $parts = parse_url($baseurl);
      $href = $parts['scheme'] . '://';
      if (isset($parts['user']) && isset($parts['pass'])) {
        $href .= $parts['user'] . ':' . $parts['pass'] . '@';
      }
      $href .= $parts['host'];
      if (isset($parts['port'])) {
        $href .= ':' . $parts['port'];
      }
      $href .= $path;
    }
  }
  return $href;
}

function _htmlsiteimport_file_exists($href) {
  $response = drupal_http_request($href, array('method' => 'HEAD'));
  if ($filesize = $response->headers['content-length']) {
    $filename = array_pop(explode('/', $href));
    if ($fid = db_query('SELECT fid FROM {file_managed} WHERE filesize = :size AND filename IN (:name)', array(':size' => $filesize, ':name' => array($filename, rawurldecode($filename))))->fetchField()) {
      return array_shift(entity_load('file', FALSE, array('fid' => $fid)));
    }
  }
}

function _htmlsiteimport_redirect($source_path, $redirect_path) {
  if ($url = parse_url($source_path)) {
    $source_path = ltrim($url['path'], '/') ?: '<front>';
    $source_options = array();
    if (!empty($url['query'])) {
      $query = array();
      parse_str($url['query'], $query);
      $source_options = array('query' => $query);
    }
    if (module_exists('redirect')) {
      if ($rid = db_query('SELECT rid FROM {redirect} WHERE source = ? AND redirect = ?', array($redirect_path, $source_path))->fetchField()) { // intentional assignment of $rid
        db_delete('redirect')->condition('rid', $rid)->execute();
      }
      if ($rid = db_query('SELECT rid FROM {redirect} WHERE source = ? and source_options = ?', array($source_path, serialize($source_options)))->fetchField()) { // intentional assignment of $rid
        db_delete('redirect')->condition('rid', $rid)->execute();
      }
      $redirect = (object) array(
        'type' => 'redirect',
        'source' => $source_path,
        'source_options' => $source_options,
        'redirect' => $redirect_path,
        'redirect_options' => array(),
        'language' => language_default('language'),
        'status_code' => 301,
      );
      redirect_save($redirect);
    }
  }
}

function htmlsiteimport_default_node_fields() {
  return array(
    array(
      'field' => 'property:title',
      'qp' => 'title',
      'selector' => 'text',
      'multiple' => 'first',
      'process' => array(
        'utf8' => 'utf8',
      ),
      'default' => 'untitled',
    ),
    array(
      'field' => 'property:uid',
      'qp' => '',
      'selector' => '',
      'multiple' => '',
      'process' => array(),
      'default' => '1',
    ),
    array(
      'field' => 'field:body:value',
      'qp' => '',
      'selector' => 'innerHTML',
      'multiple' => 'concatenate',
      'process' => array(
        'images' => 'images',
        'links' => 'links',
        'media' => 'media',
        'utf8' => 'utf8',
      ),
      'default' => '',
    ),
    array(
      'field' => 'field:body:format',
      'qp' => '',
      'selector' => '',
      'multiple' => '',
      'process' => array(),
      'default' => 'filtered_html',
    ),
    array(
      'field' => 'field:field_main_image:fid',
      'qp' => '',
      'selector' => 'src',
      'multiple' => 'first',
      'process' => array(
        'images' => 'images',
      ),
      'default' => '',
    ),
    array(
      'field' => 'field:field_main_image:alt',
      'qp' => '',
      'selector' => 'alt',
      'multiple' => 'first',
      'process' => array(),
      'default' => '',
    ),
    array(
      'field' => 'field:field_main_image:title',
      'qp' => '',
      'selector' => 'title',
      'multiple' => 'first',
      'process' => array(),
      'default' => '',
    ),
  );
}

function _htmlsiteimport_qp($item) {
  if (!empty($item->html)) {
    $qp = htmlqp($item->html);
  }
  elseif (!empty($item->url)) {
    $qp = htmlqp($item->url);
  }
  else {
    return FALSE;
  }
  
  if (!is_object($qp) || !$qp->html()) {
    drupal_set_message(t('Could not retrieve querypath object for item !url', $item->url), 'warning');
    return FALSE;
  }
  
  return $qp;
  
}

// HELPER FUNCTION FOR SAVING ENTITY
// TODO: remove if entity_save allows entity to be passed by reference

function _htmlsiteimport_entity_save($entity_type, &$entity) {
  $info = entity_get_info($entity_type);
  if (method_exists($entity, 'save')) {
    return $entity->save();
  }
  elseif (isset($info['save callback'])) {
    $info['save callback']($entity);
    return $entity->{$info['entity keys']['id']};
  }
  elseif (in_array('EntityAPIControllerInterface', class_implements($info['controller class']))) {
    return entity_get_controller($entity_type)->save($entity);
  }
  else {
    return FALSE;
  }
}
